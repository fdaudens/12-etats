var config = {
    style: 'mapbox://styles/fdaudens/ckfnblbwi08vi19o0ngdug62c',
    // style: 'mapbox://styles/fdaudens/ckfnas89l040f1arjb4azzy6b',
    accessToken: 'pk.eyJ1IjoiZmRhdWRlbnMiLCJhIjoicUtCOGRFSSJ9.JV9UlZPShWdgvloqqcVaqg',
    showMarkers: false,
    theme: 'light',
    alignment: 'left',
    title: '12 États à surveiller pour les élections américaines',
    subtitle: 'Notre sélection des luttes chaudes dans ce scrutin qui oppose Joe Biden à Donald Trump.',
    byline: 'Le Devoir',
    footer: 'Source: source citations, etc.',
    chapters: [
        {
            id: 'slug-style-id',
            title: 'INTRO',
            //image: './path/to/image/source.png',
            description: 'Un super texte.',
            location: {
                center: [-97.81515, 42.28419],
                zoom: 2.34,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                {
                    layer: 'etats-selectionnes',
                    opacity: 0.5
                }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },
        {
            id: 'Floride',
            title: 'Floride',
            //image: './path/to/image/source.png',
            description: 'Un super texte.',
            location: {
                center: [-83.78306, 27.76517],
                zoom: 5.5,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                {
                    layer: 'etats-selectionnes',
                    opacity: 0.75
                }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },
        {
            id: 'Ohio',
            title: 'Ohio',
            //image: './path/to/image/source.png',
            description: 'Un super texte.',
            location: {
                center: [-82.66957, 40.37656],
                zoom: 6,
                pitch: 0,
                bearing: 0.00
            },
            onChapterEnter: [
                {
                    layer: 'etats-selectionnes',
                    opacity: 0.75
                }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },        
        {
            id: 'Michigan',
            title: 'Michigan',
            description: 'Un super texte.',
            location: {
                center: [-86.28202, 45.13877],
                zoom: 5.5,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },
        {
            id: 'Pennsylvanie',
            title: 'Pennsylvanie',
            description: 'Un super texte.',
            location: {
                center: [-77.65356, 41.10209],
                zoom: 5.5,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },
        {
            id: 'Wisconsin',
            title: 'Wisconsin',
            description: 'Un super texte.',
            location: {
                center: [-89.59901, 44.99737],
                zoom: 5.5,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },
        {
            id: 'Arizona',
            title: 'Arizona',
            description: 'Un super texte.',
            location: {
                center: [-111.93088, 34.21568],
                zoom: 5.5,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },
        {
            id: 'Maine',
            title: 'Maine',
            description: 'Un super texte.',
            location: {
                center: [-68.98435, 45.24963],
                zoom: 5.5,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },
        {
            id: 'Minnesota',
            title: 'Minnesota',
            description: 'Un super texte.',
            location: {
                center: [-93.31176, 46.52146],
                zoom: 5.5,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },
        {
            id: 'New Hampshire',
            title: 'New Hampshire',
            description: 'Un super texte.',
            location: {
                center: [-71.53007, 44.01560],
                zoom: 6,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },
        {
            id: 'Caroline du Nord',
            title: 'Caroline du Nord',
            description: 'Un super texte.',
            location: {
                center: [-79.37793, 35.59901],
                zoom: 5.5,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },
        {
            id: 'Nevada',
            title: 'Nevada',
            description: 'Un super texte.',
            location: {
                center: [-117.02297, 38.58707],
                zoom: 5.5,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        },
        {
            id: 'Texas',
            title: 'Texas',
            description: 'Un super texte.',
            location: {
                center: [-100.07676, 31.31955],
                zoom: 4.5,
                pitch: 0.00,
                bearing: 0.00
            },
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'layer-name',
                //     opacity: 0
                // }
            ]
        }
    ]
};
